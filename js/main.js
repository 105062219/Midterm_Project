function init() {

	var messageList = document.getElementById('messages');
	var messageForm = document.getElementById('message-form');
	var InputBar = document.getElementById('message');
	var submitButton = document.getElementById('submit');
	var submitImageButton = document.getElementById('submitImage');
	var imageForm = document.getElementById('image-form');
	var mediaCapture = document.getElementById('mediaCapture');
	var signInButton = document.getElementById('sign-in');
	var signUpButton = document.getElementById('sign-up');
	var signOutButton = document.getElementById('sign-out');

	var messagesRef = firebase.database().ref('messages');
	var mymessage = false;

	var template = '<div class="message-container">' + '<div class="spacing"><div class="pic"></div></div>' + '<div class="message"></div>' + '<div class="name"></div>' + '</div>';
	var loading = 'https://www.google.com/images/spin-32.gif';

	firebase.auth().onAuthStateChanged(function(user){
		var userPic = document.getElementById('user-pic');
		var userName = document.getElementById('user-name');
		if(user){
			var profilePicUrl = user.photoURL;
			var username = user.email;
		
			userPic.style.backgroundImage = 'url(' + (profilePicUrl || '/images/profile_placeholder.png') + ')';
			userName.textContent = username;

			userName.removeAttribute('hidden');
			userPic.removeAttribute('hidden');
			signOutButton.removeAttribute('hidden');

			signUpButton.setAttribute('hidden', 'true');
			signInButton.setAttribute('hidden', 'true');

			loadMessages();
		
			firebase.messaging().requestPermission();
		} 
		else{
			userName.setAttribute('hidden', 'true');
			userPic.setAttribute('hidden', 'true');
			signOutButton.setAttribute('hidden', 'true');
			signInButton.removeAttribute('hidden');
			signUpButton.removeAttribute('hidden');
			messageList.innerHTML = '';
		}
		messagesRef.limitToLast(1).on("child_added", function(data){
			var item = data.val();
			if(item.name != user.email){
				console.log(item.name, user.email);
				var notification = new Notification("New Message!");
				notification.onclick = function () {
					window.open("https://chatroommid.firebaseapp.com/"); 
					notification.close();     
				};
			}
		});
	});

	signInButton.addEventListener('click', signIn);
	function signIn(){
		var provider = new firebase.auth.GoogleAuthProvider();
		firebase.auth().signInWithPopup(provider);
	};

	signOutButton.addEventListener('click', signout);
	function signout(){
		firebase.auth().signOut();
	};	

	function loadMessages(){
		messagesRef.off();
		var setMessage = function(data) {
			var val = data.val();
			displayMessage(data.key, val.name, val.text, val.photoUrl, val.imageUrl);
		};
		messagesRef.limitToLast(12).on('child_added', setMessage);
		messagesRef.limitToLast(12).on('child_changed', setMessage);

		messageForm.addEventListener('submit', saveMessage);
	}

	function saveMessage(evt){
		evt.preventDefault();
		if (InputBar.value && firebase.auth().currentUser) {
			var currentUser = firebase.auth().currentUser;
			messagesRef.push({
				name: currentUser.email,
				text: InputBar.value,
				photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
			}).then(function(){
				InputBar.value = '';
				InputBar.parentNode.MaterialTextfield.boundUpdateClassesHandler();
				toggleButton();
			});
		}
	}

	function setImageUrl(imageUri, imgElement){
		if (imageUri.startsWith('gs://')) {
			imgElement.src = loading;
			firebase.storage().refFromURL(imageUri).getMetadata().then(function(metadata) {
				imgElement.src = metadata.downloadURLs[0];
			});
		} 
		else{
			imgElement.src = imageUri;
		}
	}

	function displayMessage(key, name, text, picUrl, imageUri){
		var div = document.getElementById(key);
		if (!div) {
			var container = document.createElement('div');
			container.innerHTML = template;
			div = container.firstChild;
			div.setAttribute('id', key);
			messageList.appendChild(div);
		}
		if (picUrl) {
			div.querySelector('.pic').style.backgroundImage = 'url(' + picUrl + ')';
		}
		div.querySelector('.name').textContent = name;
		var messageElement = div.querySelector('.message');
		if (text){
			messageElement.textContent = text;
			messageElement.innerHTML = messageElement.innerHTML.replace(/\n/g, '<br>');
		} else if (imageUri){
			var image = document.createElement('img');
			image.addEventListener('load', function() {
				messageList.scrollTop = messageList.scrollHeight;
			});
			setImageUrl(imageUri, image);
			messageElement.innerHTML = '';
			messageElement.appendChild(image);
		}
		setTimeout(function() {div.classList.add('visible')}, 1);
		messageList.scrollTop = messageList.scrollHeight;
		InputBar.focus();
	}

	InputBar.addEventListener('keyup', toggleButton);
	InputBar.addEventListener('change', toggleButton);

	function toggleButton(){
		if (InputBar.value){
			submitButton.removeAttribute('disabled');
		} 
		else{
			submitButton.setAttribute('disabled', 'true');
		}
	};

	submitImageButton.addEventListener('click', function(evt){
		evt.preventDefault();
		mediaCapture.click();
	});
	mediaCapture.addEventListener('change', saveImageMessage);

	function saveImageMessage(evt){
		evt.preventDefault();
		var file = event.target.files[0];

		imageForm.reset();

		if (!file.type.match('image.*')) return;

		if (firebase.auth().currentUser) {

			var currentUser = firebase.auth().currentUser;
			messagesRef.push({
				name: currentUser.email,
				imageUrl: loading,
				photoUrl: currentUser.photoURL || '/images/profile_placeholder.png'
			}).then(function(data) {
				var filePath = currentUser.uid + '/' + data.key + '/' + file.name;
				return firebase.storage().ref(filePath).put(file).then(function(snapshot) {
					var fullPath = snapshot.metadata.fullPath;
					return data.update({imageUrl: firebase.storage().ref(fullPath).toString()});
				});
			})
		}
	}

	var bigeyes_grin_em = document.getElementById('bigeyes_grin');
	var sweat_grin_em = document.getElementById('sweat_grin');
	var rolling_eyes_em = document.getElementById('rolling_eyes');
	var sleepy_em = document.getElementById('sleepy');
	var swear_em = document.getElementById('swear');
	var hearty_em = document.getElementById('hearty');
	var astonish_em = document.getElementById('astonish');
	var tearful_em = document.getElementById('tearful');
	var devil_grin_em = document.getElementById('devil_grin');
	var arab_em = document.getElementById('arab');
	var chink_em = document.getElementById('chink');
	bigeyes_grin_em.addEventListener('click', em_1);
	function em_1(){
		InputBar.value += String.fromCodePoint('0x1F603');
	}
	sweat_grin_em.addEventListener('click', em_2);
	function em_2(){
		InputBar.value += String.fromCodePoint('0x1F605');
	}
	rolling_eyes_em.addEventListener('click', em_3);
	function em_3(){
		InputBar.value += String.fromCodePoint('0x1F644');
	}
	sleepy_em.addEventListener('click', em_4);
	function em_4(){
		InputBar.value += String.fromCodePoint('0x1F62A');
	}
	swear_em.addEventListener('click', em_5);
	function em_5(){
		InputBar.value += String.fromCodePoint('0x1F92C');
	}
	hearty_em.addEventListener('click', em_6);
	function em_6(){
		InputBar.value += String.fromCodePoint('0x1F60D');
	}
	astonish_em.addEventListener('click', em_7);
	function em_7(){
		InputBar.value += String.fromCodePoint('0x1F632');
	}
	tearful_em.addEventListener('click', em_8);
	function em_8(){
		InputBar.value += String.fromCodePoint('0x1F62D');
	}
	devil_grin_em.addEventListener('click', em_9);
	function em_9(){
		InputBar.value += String.fromCodePoint('0x1F608');
	}
	arab_em.addEventListener('click', em_10);
	function em_10(){
		InputBar.value += String.fromCodePoint('0x1F473');
	}
	chink_em.addEventListener('click', em_11);
	function em_11(){
		InputBar.value += String.fromCodePoint('0x1F472');
	}
};

window.onload = function() {
	init();
};