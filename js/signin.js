var useremail;
function initApp() {
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        useremail = txtEmail.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(e =>
            window.location = "index.html")
            .catch(e => window.alert(e.message),
                        email = "",
                        password = ""
            );

    });

    btnSignUp.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function () {
            window.alert("Sign up success!");
            email = "";
            password = "";
        }).catch(e => console.log(e.message));
    });
}

window.onload = function () {
    initApp();
};
