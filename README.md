# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Public Chat Room]
* Key functions (add/delete)
    1. [Type message]
    2. [Load history]
    3. [Chat with others]

* Other functions (add/delete)
    1. [Send Images]
    2. [Send Emojis]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
Firebase URL: https://chatroommid.firebaseapp.com 
* Components:  
    1. Membership, Third-Party Sign In: Google Sign In是利用 `firebase.auth.GoogleAuthProvider()` 及 `firebase.auth().signInWithPopup(provider)`，額外pop-up一個視窗讓使用者可以利用自己的google帳號去登入。而若要使用非google的帳號登入或申請帳號的話，會額外跳到一個頁面 `signin.html` 進行申請或是登入，而在申請或登入完之後再跳回主頁面。User的暱稱會是從firebase中user object取得的email address，而頭像的部分，使用google登入的使用者將會是自己google帳號裡的頭像，非google使用者則是一個事先預設好的圖片。  
    2. RWD: 這個Chat Room可以在行動裝置上使用，但礙於螢幕過小，裡面的各個components會略顯擁擠。在css裡面使用 `@media` 可以額外設定哪些元素在行動版的頁面需要顯示成什麼樣子。以這個Chat Room惟例子來說，特別將大標題、使用者名稱及登入登出、訊息版面包進 `@media` 裡面進行大小客製化。另外，在html裡面可以使用MDL(Material Design Lite)，有效利用它所提供的class，像是 `mdl-cell--N-col-tablet` 或是 `mdl-cell--N-col-desktop` 此類的class name，可以輕易設計因應裝置大小的網頁。  
    3. Notification: 會在使用者第一次登入這個網頁的時候進行request permission，如果使用者同意才會出現notification，否則不會出現。利用 `messagesRef.limitToLast(1).on("child_added"......`，在聊天室裡面出現新訊息的時候，會利用database裡訊息來源的email address判斷是否為自己的訊息還是別人的訊息，這樣才能避免自己送訊息的時候也跳出notification。  
    4. CSS Animation: 在背景使用到。在css裡面利用animation可以讓背景自己變色，這邊使用到了三種不同深度的綠色，讓animation的過程可以有漸層變化。  
    5. Functions, Database: 使用者主要可以使用的功能是傳遞訊息，表情符號或是自己上傳的圖像。進行登入之後可以看到聊天紀錄(在登入之前是空白的，登出之後聊天紀錄會再度清空)。在聊天室裡面的使用者可以送出自己在message的input field裡面打的字，或是可以選擇表情列表裡面的emoji，也可以選擇自己電腦上的圖片。這些訊息在送出的同時，會被當作child加進database裡 `messages`。上傳圖片的部分，是先將使用者端的圖片抓到storage裡面，再從storage顯示該圖片。  

## Security Report (Optional)
1. 聊天室裡的紀錄需要在登入之後才能閱覽，登出之後會再度消失。
2. storage.rules裡片有限制，bucket裡面如果不是自己的folder不能write進去。